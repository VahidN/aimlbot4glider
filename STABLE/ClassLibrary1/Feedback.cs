﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using AIML;
using System.Threading;

namespace AIML
{
    class Feedback
    {
        static string TempMessage = "";

        public static void Send(string msg)
        {
            TempMessage = msg;
            Thread SendFeed = new Thread(new ThreadStart(Send));
            SendFeed.Start();
        }

        public static void Send()
        {
            string msg = TempMessage;
            try
            {
                ChatBot.AIMLBOTLog("Could not find a answer for: " + msg, true);

                //The method we will use to send the data, this can be POST or GET.  
                string requestmethod = "POST";

                //Here we will enter the data to send, just like if we where to go to a webpage and enter variables,  
                // we would type: "www.somesite.com?var1=Hello&var2=Server!"!  
                string postData = "var1=password&var2=" + msg;

                //The Byte Array that will be used for writing the data to the stream.  
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                //The URL of the webpage to send the data to.  
                string URL = "http://aimbot.webhop.net/Feedback.php";

                //The type of content being send, this is almost always "application/x-www-form-urlencoded".  
                string contenttype = "application/x-www-form-urlencoded";

                //What the server sends back:  
                string responseFromServer = null;

                //Here we will create the WebRequest object, and enter the URL as soon as it is created.  
                WebRequest request = WebRequest.Create(URL);

                //We also need a Stream:  
                Stream dataStream;

                //...And a webResponce,  
                WebResponse response;

                //don't forget the streamreader either!  
                StreamReader reader;

                //We will need to set the method used to send the data.  
                request.Method = requestmethod;

                //Then the contenttype:  
                request.ContentType = contenttype;

                //content length  
                request.ContentLength = byteArray.Length;

                //ok, now get the request from the webRequest object, and put it into our Stream:  
                dataStream = request.GetRequestStream();

                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);

                // Close the Stream object.  
                dataStream.Close();

                //Get the responce  
                response = request.GetResponse();

                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();

                //Open the responce stream:  
                reader = new StreamReader(dataStream);

                //read the content into the responcefromserver string  
                responseFromServer = reader.ReadToEnd();

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                //Now, display the responce!  
                ChatBot.AIMLBOTLog(responseFromServer, false);

                //Done! 
            } catch (Exception e)
            {
                ChatBot.AIMLBOTLog("Could not send information, see the log for details");
                ChatBot.AIMLBOTLog(e.Message, true);
                ChatBot.AIMLfeedback = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using AIML;
using Glider.Common.Objects;

namespace AIML
{

    public partial class MonitorBox1 : Form
    {
        Form Form6 = new Form6();

        public MonitorBox1()
        {
            InitializeComponent();
        }

        public void AddToLog(string mesg)
        {
            textBox1.AppendText(mesg + Environment.NewLine);
        }

        private void MonitorBox_Load(object sender, EventArgs e)
        {
            if (GContext.Main.GetConfigBool("AIMLBot.AutoMSN")) Form6.Show();
            Form6.Visible = false;
            Form6.Text = GContext.Main.GetRandomString();
            label2.Text = ChatBot.Version;
            this.Location = new Point(GContext.Main.GetConfigInt("AIMLBot.PosX"), GContext.Main.GetConfigInt("AIMLBot.PosY"));
            ChatBot.GetForum(this);

            if (GContext.Main.GetConfigString("AIMLBot.AIMLuse") == "True")
            {
                automaticResponseToolStripMenuItem.Checked = true;
                ChatBot.AIMLautoanswer = true;
            }
            else
            {
                automaticResponseToolStripMenuItem.Checked = true;
                ChatBot.AIMLautoanswer = true;
            }

        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void onlineSupportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://vforums.mmoglider.com/showthread.php?t=172261");
        }

        private void basicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form1 = new Form1();
            form1.Text = GContext.Main.GetRandomString();
            form1.Location = this.Location;
            form1.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!GContext.Main.IsGliderRunning)
            {
                ChatBot.AIMLBOTLog("AIMLbot4Glider is not running under Glider, stopping? Did Glider crash?", true);
                ChatBot.UnLoader();
            }
            
            if (GContext.Main.IsGliding)
            {
                label3.Text = GContext.Main.Me.Name;
            } else label3.Text = "Idle";


            int tmp = (ChatBot.BlacklistCount * 100 / ChatBot.MaxBlacklistCount) + 1;
            if (tmp >= 100) tmp = 100;

            progressBar1.Value = tmp;

            if (progressBar1.Value <= 35) progressBar1.ForeColor = Color.Green;
            if (progressBar1.Value >= 35) progressBar1.ForeColor = Color.Orange;
            if (progressBar1.Value >= 70) progressBar1.ForeColor = Color.Red;

            if (ChatBot.LastFrom.Length >= 1 && ChatBot.ManualChatWith.Length <= 1)
                {
                    button2.Text = "Manual (" + ChatBot.LastFrom + ")";
                    button2.Enabled = true;
                }

            if (ChatBot.AIMLautoanswer)
            {
                automaticResponseToolStripMenuItem.Checked = true;
                textBox1.BackColor = Color.LemonChiffon;
                label4.Visible = false;
            }
            else
            {
                automaticResponseToolStripMenuItem.Checked = false;
                textBox1.BackColor = Color.Red;
                if (label4.Visible) label4.Visible = false; else label4.Visible = true;
            }

            if (ChatBot.ManualChatWith.Length >= 1) button2.Enabled = false;

            string AppTime = "";
            if (ChatBot.AppriciatTimer.TicksLeft <= 1) AppTime = "Ready";
            else AppTime = Convert.ToString(ChatBot.AppriciatTimer.TicksLeft / 1000);

            string RepTime = "";
            if (ChatBot.CleanMessageSendTimer.TicksLeft <= 1) RepTime = "Clear";
            else RepTime = Convert.ToString(ChatBot.CleanMessageSendTimer.TicksLeft / 1000);

            string BGTime = "";
            if (ChatBot.BGTimer.TicksLeft <= 1) BGTime = "Ready";
            else BGTime = Convert.ToString(ChatBot.BGTimer.TicksLeft / 1000);

            toolStripStatusLabel1.Text = "Appriciation : " + AppTime;
            toolStripStatusLabel2.Text = "Repeating : " + RepTime;
            toolStripStatusLabel3.Text = "BG : " + BGTime;
        }

        private void advancedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form2 = new Form2();
            form2.Text = GContext.Main.GetRandomString();
            form2.Location = this.Location;
            form2.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void sendLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form SendLine = new SendLine();
            SendLine.Text = GContext.Main.GetRandomString();
            SendLine.Location = this.Location;
            SendLine.Show();
        }

        private void showAppriciationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GContext.Main.IsAttached) ChatBot.SendAppriciation();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            ChatBot.BlacklistCount = 0;
            progressBar1.Value = 1;
        }

        private void blackListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form3 = new Form3();
            form3.Text = GContext.Main.GetRandomString();
            form3.Location = this.Location;
            form3.Show();
        }

        private void chatWithAIMLbotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form Form4 = new Form4();
            Form4.Text = GContext.Main.GetRandomString();
            Form4.Location = this.Location;
            Form4.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChatBot.UnLoader();
        }

        private void resetBlacklistCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChatBot.BlacklistCount = 0;
            progressBar1.Value = 1;
        }

        private void textBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe", AppDomain.CurrentDomain.BaseDirectory + "Classes\\AIMLbot4Glider\\AIMLbot.Log");
        }

        private void logfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe", AppDomain.CurrentDomain.BaseDirectory + "Classes\\AIMLbot4Glider\\AIMLbot.Log");
        }

        private void MonitorBox1_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "Manual (none)") return;

            ChatBot.ManualChatWith = ChatBot.LastFrom;
            Form form5 = new Form5();
            form5.Text = GContext.Main.GetRandomString();
            form5.Location = this.Location;
            form5.Show();
            button2.Enabled = false;
        }

        private void mSNControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form6.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {   
            if (Form6.Visible != true) Form6.Visible = true; else Form6.Visible = false;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
            notifyIcon1.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }

        private void MonitorBox1_Resize(object sender, EventArgs e)
        {
        if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                this.Visible = false;
                notifyIcon1.ShowBalloonTip(500);
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                this.Visible = true;
                notifyIcon1.Visible = false;
            }
        }

        private void MonitorBox1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ChatBot.TryIcon)
            {
                notifyIcon1.Visible = true;
                this.Visible = false;
                notifyIcon1.ShowBalloonTip(5000);
                e.Cancel = true;
            }   
        }

        private void showAIMLbot4GliderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void mSNControlToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form6.Visible = true;
        }

        private void showAppriciationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (GContext.Main.IsAttached) ChatBot.SendAppriciation();
        }

        private void helpToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void downloadNewAIMLFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChatBot.GetLatestAIML();
        }

        private void automaticResponseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (automaticResponseToolStripMenuItem.Checked)
            {
                automaticResponseToolStripMenuItem.Checked = false;
                ChatBot.AIMLautoanswer = false;
            }
            else
            {
                automaticResponseToolStripMenuItem.Checked = true;
                ChatBot.AIMLautoanswer = true;
            }
        }

        string CheckboxToString(bool tmp)
        {
            if (tmp) return "True"; else return "False";
        }

        private void MonitorBox1_FormClosed(object sender, FormClosedEventArgs e)
        {
            GContext.Main.SetConfigValue("AIMLBot.AIMLuse", CheckboxToString(automaticResponseToolStripMenuItem.Checked), true);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (ChatBot.AIMLfeedback) Feedback.Send("test answer");
        }

        private void reloadAIMLFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Loading AIML files.", false);
            ChatBot.myBot.loadAIMLFromFiles();
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force Repeating timer to reset", false);
            ChatBot.CleanMessageSendTimer.Reset();
        }

        private void toolStripStatusLabel3_Click(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force BG timer to reset", false);
            ChatBot.BGTimer.Reset();
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force Appriciation timer to reset", false);
            ChatBot.AppriciatTimer.Reset();
        }

        private void toolStripStatusLabel1_DoubleClick(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force Appriciation timer to be ready", false);
            ChatBot.AppriciatTimer.ForceReady();
        }

        private void toolStripStatusLabel3_DoubleClick(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force BG timer to be ready", false);
            ChatBot.BGTimer.ForceReady();
        }

        private void toolStripStatusLabel2_DoubleClick(object sender, EventArgs e)
        {
            ChatBot.AIMLBOTLog("Force Repeating timer to be ready", false);
            ChatBot.CleanMessageSendTimer.ForceReady();
        }
    }
}

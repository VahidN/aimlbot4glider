﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AIMLbot4Glider")]
[assembly: AssemblyDescription("A in-game automated chat tool for MMOGLIDER")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PRIVATE")]
[assembly: AssemblyProduct("Zeo80")]
[assembly: AssemblyCopyright("Artistic License/GPL")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.1.1.0")]
[assembly: AssemblyFileVersion("1.1.1.0")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6b9898cd-5fc0-493a-8fea-d78f881a4baa")]


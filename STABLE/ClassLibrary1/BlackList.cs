﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AIML
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }   

        private void Form3_Load(object sender, EventArgs e)
        {
            foreach (string mesg in ChatBot.BlackList)
            {
                textBox1.AppendText(mesg + Environment.NewLine);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string boxtext = textBox1.Text;
            string[] split = boxtext.Split(new char[] { '\n' });

            ChatBot.BlackList.Clear();

            foreach (string mesg in split)
            {
                if (mesg.Length >= 1) ChatBot.BlackList.Add(mesg);
            }
            this.Hide();
        }

        private void Form3_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }
    }
}

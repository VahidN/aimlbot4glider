﻿namespace AIML
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.textBox112 = new System.Windows.Forms.TextBox();
            this.textBox111 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox140 = new System.Windows.Forms.TextBox();
            this.textBox139 = new System.Windows.Forms.TextBox();
            this.textBox138 = new System.Windows.Forms.TextBox();
            this.textBox137 = new System.Windows.Forms.TextBox();
            this.textBox136 = new System.Windows.Forms.TextBox();
            this.textBox135 = new System.Windows.Forms.TextBox();
            this.textBox134 = new System.Windows.Forms.TextBox();
            this.textBox133 = new System.Windows.Forms.TextBox();
            this.textBox132 = new System.Windows.Forms.TextBox();
            this.textBox131 = new System.Windows.Forms.TextBox();
            this.textBox130 = new System.Windows.Forms.TextBox();
            this.textBox129 = new System.Windows.Forms.TextBox();
            this.textBox128 = new System.Windows.Forms.TextBox();
            this.textBox127 = new System.Windows.Forms.TextBox();
            this.textBox126 = new System.Windows.Forms.TextBox();
            this.textBox125 = new System.Windows.Forms.TextBox();
            this.textBox124 = new System.Windows.Forms.TextBox();
            this.textBox123 = new System.Windows.Forms.TextBox();
            this.textBox122 = new System.Windows.Forms.TextBox();
            this.textBox121 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(317, 362);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "&OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(480, 362);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "&Help";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(399, 362);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "&Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(12, 14);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(543, 342);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage1.Controls.Add(this.textBox20);
            this.tabPage1.Controls.Add(this.textBox19);
            this.tabPage1.Controls.Add(this.textBox18);
            this.tabPage1.Controls.Add(this.textBox17);
            this.tabPage1.Controls.Add(this.textBox16);
            this.tabPage1.Controls.Add(this.textBox15);
            this.tabPage1.Controls.Add(this.textBox14);
            this.tabPage1.Controls.Add(this.textBox13);
            this.tabPage1.Controls.Add(this.textBox12);
            this.tabPage1.Controls.Add(this.textBox11);
            this.tabPage1.Controls.Add(this.textBox10);
            this.tabPage1.Controls.Add(this.textBox9);
            this.tabPage1.Controls.Add(this.textBox8);
            this.tabPage1.Controls.Add(this.textBox7);
            this.tabPage1.Controls.Add(this.textBox6);
            this.tabPage1.Controls.Add(this.textBox5);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(535, 316);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Appriciations";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(290, 257);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(225, 20);
            this.textBox20.TabIndex = 19;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(290, 231);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(225, 20);
            this.textBox19.TabIndex = 18;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(290, 205);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(225, 20);
            this.textBox18.TabIndex = 17;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(290, 179);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(225, 20);
            this.textBox17.TabIndex = 16;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(290, 153);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(225, 20);
            this.textBox16.TabIndex = 15;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(290, 127);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(225, 20);
            this.textBox15.TabIndex = 14;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(290, 101);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(225, 20);
            this.textBox14.TabIndex = 13;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(290, 75);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(225, 20);
            this.textBox13.TabIndex = 12;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(290, 49);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(225, 20);
            this.textBox12.TabIndex = 11;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(290, 23);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(225, 20);
            this.textBox11.TabIndex = 10;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(19, 257);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(225, 20);
            this.textBox10.TabIndex = 9;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(19, 231);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(225, 20);
            this.textBox9.TabIndex = 8;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(19, 205);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(225, 20);
            this.textBox8.TabIndex = 7;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(19, 179);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(225, 20);
            this.textBox7.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(19, 153);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(225, 20);
            this.textBox6.TabIndex = 5;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(19, 127);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(225, 20);
            this.textBox5.TabIndex = 4;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(19, 101);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(225, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(19, 75);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(225, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(19, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(225, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(19, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 20);
            this.textBox1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage2.Controls.Add(this.textBox40);
            this.tabPage2.Controls.Add(this.textBox39);
            this.tabPage2.Controls.Add(this.textBox38);
            this.tabPage2.Controls.Add(this.textBox37);
            this.tabPage2.Controls.Add(this.textBox36);
            this.tabPage2.Controls.Add(this.textBox35);
            this.tabPage2.Controls.Add(this.textBox34);
            this.tabPage2.Controls.Add(this.textBox33);
            this.tabPage2.Controls.Add(this.textBox32);
            this.tabPage2.Controls.Add(this.textBox31);
            this.tabPage2.Controls.Add(this.textBox30);
            this.tabPage2.Controls.Add(this.textBox29);
            this.tabPage2.Controls.Add(this.textBox28);
            this.tabPage2.Controls.Add(this.textBox27);
            this.tabPage2.Controls.Add(this.textBox26);
            this.tabPage2.Controls.Add(this.textBox25);
            this.tabPage2.Controls.Add(this.textBox24);
            this.tabPage2.Controls.Add(this.textBox23);
            this.tabPage2.Controls.Add(this.textBox22);
            this.tabPage2.Controls.Add(this.textBox21);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(535, 316);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Corrections";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(290, 257);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(225, 20);
            this.textBox40.TabIndex = 19;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(290, 231);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(225, 20);
            this.textBox39.TabIndex = 18;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(290, 205);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(225, 20);
            this.textBox38.TabIndex = 17;
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(290, 179);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(225, 20);
            this.textBox37.TabIndex = 16;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(290, 153);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(225, 20);
            this.textBox36.TabIndex = 15;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(290, 127);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(225, 20);
            this.textBox35.TabIndex = 14;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(290, 101);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(225, 20);
            this.textBox34.TabIndex = 13;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(290, 75);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(225, 20);
            this.textBox33.TabIndex = 12;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(290, 49);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(225, 20);
            this.textBox32.TabIndex = 11;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(290, 23);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(225, 20);
            this.textBox31.TabIndex = 10;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(19, 257);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(225, 20);
            this.textBox30.TabIndex = 9;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(19, 231);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(225, 20);
            this.textBox29.TabIndex = 8;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(19, 205);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(225, 20);
            this.textBox28.TabIndex = 7;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(19, 179);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(225, 20);
            this.textBox27.TabIndex = 6;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(19, 153);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(225, 20);
            this.textBox26.TabIndex = 5;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(19, 127);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(225, 20);
            this.textBox25.TabIndex = 4;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(19, 101);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(225, 20);
            this.textBox24.TabIndex = 3;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(19, 75);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(225, 20);
            this.textBox23.TabIndex = 2;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(19, 49);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(225, 20);
            this.textBox22.TabIndex = 1;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(19, 23);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(225, 20);
            this.textBox21.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage3.Controls.Add(this.textBox60);
            this.tabPage3.Controls.Add(this.textBox59);
            this.tabPage3.Controls.Add(this.textBox58);
            this.tabPage3.Controls.Add(this.textBox57);
            this.tabPage3.Controls.Add(this.textBox56);
            this.tabPage3.Controls.Add(this.textBox55);
            this.tabPage3.Controls.Add(this.textBox54);
            this.tabPage3.Controls.Add(this.textBox53);
            this.tabPage3.Controls.Add(this.textBox52);
            this.tabPage3.Controls.Add(this.textBox51);
            this.tabPage3.Controls.Add(this.textBox50);
            this.tabPage3.Controls.Add(this.textBox49);
            this.tabPage3.Controls.Add(this.textBox48);
            this.tabPage3.Controls.Add(this.textBox47);
            this.tabPage3.Controls.Add(this.textBox46);
            this.tabPage3.Controls.Add(this.textBox45);
            this.tabPage3.Controls.Add(this.textBox44);
            this.tabPage3.Controls.Add(this.textBox43);
            this.tabPage3.Controls.Add(this.textBox42);
            this.tabPage3.Controls.Add(this.textBox41);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(535, 316);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Invites";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(290, 257);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(225, 20);
            this.textBox60.TabIndex = 19;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(290, 231);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(225, 20);
            this.textBox59.TabIndex = 18;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(290, 205);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(225, 20);
            this.textBox58.TabIndex = 17;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(290, 179);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(225, 20);
            this.textBox57.TabIndex = 16;
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(290, 153);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(225, 20);
            this.textBox56.TabIndex = 15;
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(290, 127);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(225, 20);
            this.textBox55.TabIndex = 14;
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(290, 101);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(225, 20);
            this.textBox54.TabIndex = 13;
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(290, 75);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(225, 20);
            this.textBox53.TabIndex = 12;
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(290, 49);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(225, 20);
            this.textBox52.TabIndex = 11;
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(290, 23);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(225, 20);
            this.textBox51.TabIndex = 10;
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(19, 257);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(225, 20);
            this.textBox50.TabIndex = 9;
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(19, 231);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(225, 20);
            this.textBox49.TabIndex = 8;
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(19, 205);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(225, 20);
            this.textBox48.TabIndex = 7;
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(19, 179);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(225, 20);
            this.textBox47.TabIndex = 6;
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(19, 153);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(225, 20);
            this.textBox46.TabIndex = 5;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(19, 127);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(225, 20);
            this.textBox45.TabIndex = 4;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(19, 101);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(225, 20);
            this.textBox44.TabIndex = 3;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(19, 75);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(225, 20);
            this.textBox43.TabIndex = 2;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(19, 49);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(225, 20);
            this.textBox42.TabIndex = 1;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(19, 23);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(225, 20);
            this.textBox41.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage4.Controls.Add(this.textBox80);
            this.tabPage4.Controls.Add(this.textBox79);
            this.tabPage4.Controls.Add(this.textBox78);
            this.tabPage4.Controls.Add(this.textBox77);
            this.tabPage4.Controls.Add(this.textBox76);
            this.tabPage4.Controls.Add(this.textBox75);
            this.tabPage4.Controls.Add(this.textBox74);
            this.tabPage4.Controls.Add(this.textBox73);
            this.tabPage4.Controls.Add(this.textBox72);
            this.tabPage4.Controls.Add(this.textBox71);
            this.tabPage4.Controls.Add(this.textBox70);
            this.tabPage4.Controls.Add(this.textBox69);
            this.tabPage4.Controls.Add(this.textBox68);
            this.tabPage4.Controls.Add(this.textBox67);
            this.tabPage4.Controls.Add(this.textBox66);
            this.tabPage4.Controls.Add(this.textBox65);
            this.tabPage4.Controls.Add(this.textBox64);
            this.tabPage4.Controls.Add(this.textBox63);
            this.tabPage4.Controls.Add(this.textBox62);
            this.tabPage4.Controls.Add(this.textBox61);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(535, 316);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Openings";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // textBox80
            // 
            this.textBox80.Location = new System.Drawing.Point(290, 257);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(225, 20);
            this.textBox80.TabIndex = 19;
            // 
            // textBox79
            // 
            this.textBox79.Location = new System.Drawing.Point(290, 231);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(225, 20);
            this.textBox79.TabIndex = 18;
            // 
            // textBox78
            // 
            this.textBox78.Location = new System.Drawing.Point(290, 205);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(225, 20);
            this.textBox78.TabIndex = 17;
            // 
            // textBox77
            // 
            this.textBox77.Location = new System.Drawing.Point(290, 179);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(225, 20);
            this.textBox77.TabIndex = 16;
            // 
            // textBox76
            // 
            this.textBox76.Location = new System.Drawing.Point(290, 153);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(225, 20);
            this.textBox76.TabIndex = 15;
            // 
            // textBox75
            // 
            this.textBox75.Location = new System.Drawing.Point(290, 127);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(225, 20);
            this.textBox75.TabIndex = 14;
            // 
            // textBox74
            // 
            this.textBox74.Location = new System.Drawing.Point(290, 101);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(225, 20);
            this.textBox74.TabIndex = 13;
            // 
            // textBox73
            // 
            this.textBox73.Location = new System.Drawing.Point(290, 75);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(225, 20);
            this.textBox73.TabIndex = 12;
            // 
            // textBox72
            // 
            this.textBox72.Location = new System.Drawing.Point(290, 49);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(225, 20);
            this.textBox72.TabIndex = 11;
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(290, 23);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(225, 20);
            this.textBox71.TabIndex = 10;
            // 
            // textBox70
            // 
            this.textBox70.Location = new System.Drawing.Point(19, 257);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(225, 20);
            this.textBox70.TabIndex = 9;
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(19, 231);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(225, 20);
            this.textBox69.TabIndex = 8;
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(19, 205);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(225, 20);
            this.textBox68.TabIndex = 7;
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(19, 179);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(225, 20);
            this.textBox67.TabIndex = 6;
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(19, 153);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(225, 20);
            this.textBox66.TabIndex = 5;
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(19, 127);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(225, 20);
            this.textBox65.TabIndex = 4;
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(19, 101);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(225, 20);
            this.textBox64.TabIndex = 3;
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(19, 75);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(225, 20);
            this.textBox63.TabIndex = 2;
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(19, 49);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(225, 20);
            this.textBox62.TabIndex = 1;
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(19, 23);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(225, 20);
            this.textBox61.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage5.Controls.Add(this.textBox100);
            this.tabPage5.Controls.Add(this.textBox99);
            this.tabPage5.Controls.Add(this.textBox98);
            this.tabPage5.Controls.Add(this.textBox97);
            this.tabPage5.Controls.Add(this.textBox96);
            this.tabPage5.Controls.Add(this.textBox95);
            this.tabPage5.Controls.Add(this.textBox94);
            this.tabPage5.Controls.Add(this.textBox93);
            this.tabPage5.Controls.Add(this.textBox92);
            this.tabPage5.Controls.Add(this.textBox91);
            this.tabPage5.Controls.Add(this.textBox90);
            this.tabPage5.Controls.Add(this.textBox89);
            this.tabPage5.Controls.Add(this.textBox88);
            this.tabPage5.Controls.Add(this.textBox87);
            this.tabPage5.Controls.Add(this.textBox86);
            this.tabPage5.Controls.Add(this.textBox85);
            this.tabPage5.Controls.Add(this.textBox84);
            this.tabPage5.Controls.Add(this.textBox83);
            this.tabPage5.Controls.Add(this.textBox82);
            this.tabPage5.Controls.Add(this.textBox81);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(535, 316);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Battleground (Random)";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBox100
            // 
            this.textBox100.Location = new System.Drawing.Point(290, 257);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(225, 20);
            this.textBox100.TabIndex = 20;
            // 
            // textBox99
            // 
            this.textBox99.Location = new System.Drawing.Point(290, 231);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(225, 20);
            this.textBox99.TabIndex = 19;
            // 
            // textBox98
            // 
            this.textBox98.Location = new System.Drawing.Point(290, 205);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(225, 20);
            this.textBox98.TabIndex = 18;
            // 
            // textBox97
            // 
            this.textBox97.Location = new System.Drawing.Point(290, 179);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(225, 20);
            this.textBox97.TabIndex = 17;
            // 
            // textBox96
            // 
            this.textBox96.Location = new System.Drawing.Point(290, 153);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(225, 20);
            this.textBox96.TabIndex = 16;
            // 
            // textBox95
            // 
            this.textBox95.Location = new System.Drawing.Point(290, 127);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(225, 20);
            this.textBox95.TabIndex = 15;
            // 
            // textBox94
            // 
            this.textBox94.Location = new System.Drawing.Point(290, 101);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(225, 20);
            this.textBox94.TabIndex = 14;
            // 
            // textBox93
            // 
            this.textBox93.Location = new System.Drawing.Point(290, 75);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(225, 20);
            this.textBox93.TabIndex = 13;
            // 
            // textBox92
            // 
            this.textBox92.Location = new System.Drawing.Point(290, 49);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(225, 20);
            this.textBox92.TabIndex = 12;
            // 
            // textBox91
            // 
            this.textBox91.Location = new System.Drawing.Point(290, 23);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(225, 20);
            this.textBox91.TabIndex = 11;
            // 
            // textBox90
            // 
            this.textBox90.Location = new System.Drawing.Point(19, 257);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(225, 20);
            this.textBox90.TabIndex = 10;
            // 
            // textBox89
            // 
            this.textBox89.Location = new System.Drawing.Point(19, 231);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(225, 20);
            this.textBox89.TabIndex = 9;
            // 
            // textBox88
            // 
            this.textBox88.Location = new System.Drawing.Point(19, 205);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(225, 20);
            this.textBox88.TabIndex = 8;
            // 
            // textBox87
            // 
            this.textBox87.Location = new System.Drawing.Point(19, 179);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(225, 20);
            this.textBox87.TabIndex = 7;
            // 
            // textBox86
            // 
            this.textBox86.Location = new System.Drawing.Point(19, 153);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(225, 20);
            this.textBox86.TabIndex = 6;
            // 
            // textBox85
            // 
            this.textBox85.Location = new System.Drawing.Point(19, 127);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(225, 20);
            this.textBox85.TabIndex = 5;
            // 
            // textBox84
            // 
            this.textBox84.Location = new System.Drawing.Point(19, 101);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(225, 20);
            this.textBox84.TabIndex = 4;
            // 
            // textBox83
            // 
            this.textBox83.Location = new System.Drawing.Point(19, 75);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(225, 20);
            this.textBox83.TabIndex = 3;
            // 
            // textBox82
            // 
            this.textBox82.Location = new System.Drawing.Point(19, 49);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(225, 20);
            this.textBox82.TabIndex = 2;
            // 
            // textBox81
            // 
            this.textBox81.Location = new System.Drawing.Point(19, 23);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(225, 20);
            this.textBox81.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage6.Controls.Add(this.groupBox2);
            this.tabPage6.Controls.Add(this.groupBox1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(535, 316);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Battleground (Events)";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.textBox120);
            this.groupBox2.Controls.Add(this.textBox118);
            this.groupBox2.Controls.Add(this.textBox119);
            this.groupBox2.Controls.Add(this.textBox117);
            this.groupBox2.Controls.Add(this.textBox116);
            this.groupBox2.Controls.Add(this.textBox115);
            this.groupBox2.Controls.Add(this.textBox114);
            this.groupBox2.Controls.Add(this.textBox113);
            this.groupBox2.Controls.Add(this.textBox112);
            this.groupBox2.Controls.Add(this.textBox111);
            this.groupBox2.Location = new System.Drawing.Point(3, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(529, 151);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Negative events";
            // 
            // textBox120
            // 
            this.textBox120.Location = new System.Drawing.Point(298, 123);
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new System.Drawing.Size(225, 20);
            this.textBox120.TabIndex = 11;
            // 
            // textBox118
            // 
            this.textBox118.Location = new System.Drawing.Point(298, 71);
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new System.Drawing.Size(225, 20);
            this.textBox118.TabIndex = 19;
            // 
            // textBox119
            // 
            this.textBox119.Location = new System.Drawing.Point(298, 97);
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new System.Drawing.Size(225, 20);
            this.textBox119.TabIndex = 10;
            // 
            // textBox117
            // 
            this.textBox117.Location = new System.Drawing.Point(298, 45);
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new System.Drawing.Size(225, 20);
            this.textBox117.TabIndex = 18;
            // 
            // textBox116
            // 
            this.textBox116.Location = new System.Drawing.Point(298, 19);
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new System.Drawing.Size(225, 20);
            this.textBox116.TabIndex = 17;
            // 
            // textBox115
            // 
            this.textBox115.Location = new System.Drawing.Point(6, 125);
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new System.Drawing.Size(225, 20);
            this.textBox115.TabIndex = 16;
            // 
            // textBox114
            // 
            this.textBox114.Location = new System.Drawing.Point(6, 97);
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new System.Drawing.Size(225, 20);
            this.textBox114.TabIndex = 15;
            // 
            // textBox113
            // 
            this.textBox113.Location = new System.Drawing.Point(6, 71);
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new System.Drawing.Size(225, 20);
            this.textBox113.TabIndex = 14;
            // 
            // textBox112
            // 
            this.textBox112.Location = new System.Drawing.Point(6, 45);
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new System.Drawing.Size(225, 20);
            this.textBox112.TabIndex = 13;
            // 
            // textBox111
            // 
            this.textBox111.Location = new System.Drawing.Point(6, 19);
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new System.Drawing.Size(225, 20);
            this.textBox111.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.textBox108);
            this.groupBox1.Controls.Add(this.textBox107);
            this.groupBox1.Controls.Add(this.textBox106);
            this.groupBox1.Controls.Add(this.textBox105);
            this.groupBox1.Controls.Add(this.textBox104);
            this.groupBox1.Controls.Add(this.textBox103);
            this.groupBox1.Controls.Add(this.textBox102);
            this.groupBox1.Controls.Add(this.textBox101);
            this.groupBox1.Controls.Add(this.textBox109);
            this.groupBox1.Controls.Add(this.textBox110);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 153);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Positive events";
            // 
            // textBox108
            // 
            this.textBox108.Location = new System.Drawing.Point(298, 71);
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new System.Drawing.Size(225, 20);
            this.textBox108.TabIndex = 9;
            // 
            // textBox107
            // 
            this.textBox107.Location = new System.Drawing.Point(298, 45);
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new System.Drawing.Size(225, 20);
            this.textBox107.TabIndex = 8;
            // 
            // textBox106
            // 
            this.textBox106.Location = new System.Drawing.Point(298, 19);
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new System.Drawing.Size(225, 20);
            this.textBox106.TabIndex = 7;
            // 
            // textBox105
            // 
            this.textBox105.Location = new System.Drawing.Point(6, 123);
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new System.Drawing.Size(225, 20);
            this.textBox105.TabIndex = 6;
            // 
            // textBox104
            // 
            this.textBox104.Location = new System.Drawing.Point(6, 97);
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new System.Drawing.Size(225, 20);
            this.textBox104.TabIndex = 5;
            // 
            // textBox103
            // 
            this.textBox103.Location = new System.Drawing.Point(6, 71);
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(225, 20);
            this.textBox103.TabIndex = 4;
            // 
            // textBox102
            // 
            this.textBox102.Location = new System.Drawing.Point(6, 45);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(225, 20);
            this.textBox102.TabIndex = 3;
            // 
            // textBox101
            // 
            this.textBox101.Location = new System.Drawing.Point(6, 19);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(225, 20);
            this.textBox101.TabIndex = 2;
            // 
            // textBox109
            // 
            this.textBox109.Location = new System.Drawing.Point(298, 97);
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new System.Drawing.Size(225, 20);
            this.textBox109.TabIndex = 10;
            // 
            // textBox110
            // 
            this.textBox110.Location = new System.Drawing.Point(298, 123);
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new System.Drawing.Size(225, 20);
            this.textBox110.TabIndex = 11;
            // 
            // tabPage7
            // 
            this.tabPage7.BackgroundImage = global::AIML.Properties.Resources.background;
            this.tabPage7.Controls.Add(this.linkLabel1);
            this.tabPage7.Controls.Add(this.label1);
            this.tabPage7.Controls.Add(this.textBox140);
            this.tabPage7.Controls.Add(this.textBox139);
            this.tabPage7.Controls.Add(this.textBox138);
            this.tabPage7.Controls.Add(this.textBox137);
            this.tabPage7.Controls.Add(this.textBox136);
            this.tabPage7.Controls.Add(this.textBox135);
            this.tabPage7.Controls.Add(this.textBox134);
            this.tabPage7.Controls.Add(this.textBox133);
            this.tabPage7.Controls.Add(this.textBox132);
            this.tabPage7.Controls.Add(this.textBox131);
            this.tabPage7.Controls.Add(this.textBox130);
            this.tabPage7.Controls.Add(this.textBox129);
            this.tabPage7.Controls.Add(this.textBox128);
            this.tabPage7.Controls.Add(this.textBox127);
            this.tabPage7.Controls.Add(this.textBox126);
            this.tabPage7.Controls.Add(this.textBox125);
            this.tabPage7.Controls.Add(this.textBox124);
            this.tabPage7.Controls.Add(this.textBox123);
            this.tabPage7.Controls.Add(this.textBox122);
            this.tabPage7.Controls.Add(this.textBox121);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(535, 316);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Duel";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.DarkRed;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.linkLabel1.Location = new System.Drawing.Point(325, 287);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(92, 16);
            this.linkLabel1.TabIndex = 23;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "AutoDecline";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(110, 287);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 16);
            this.label1.TabIndex = 22;
            this.label1.Text = "Only works in combination with";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBox140
            // 
            this.textBox140.Location = new System.Drawing.Point(290, 257);
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new System.Drawing.Size(225, 20);
            this.textBox140.TabIndex = 21;
            // 
            // textBox139
            // 
            this.textBox139.Location = new System.Drawing.Point(290, 231);
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new System.Drawing.Size(225, 20);
            this.textBox139.TabIndex = 20;
            // 
            // textBox138
            // 
            this.textBox138.Location = new System.Drawing.Point(290, 205);
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new System.Drawing.Size(225, 20);
            this.textBox138.TabIndex = 19;
            // 
            // textBox137
            // 
            this.textBox137.Location = new System.Drawing.Point(290, 179);
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new System.Drawing.Size(225, 20);
            this.textBox137.TabIndex = 18;
            // 
            // textBox136
            // 
            this.textBox136.Location = new System.Drawing.Point(290, 153);
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new System.Drawing.Size(225, 20);
            this.textBox136.TabIndex = 17;
            // 
            // textBox135
            // 
            this.textBox135.Location = new System.Drawing.Point(290, 127);
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new System.Drawing.Size(225, 20);
            this.textBox135.TabIndex = 16;
            // 
            // textBox134
            // 
            this.textBox134.Location = new System.Drawing.Point(290, 101);
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new System.Drawing.Size(225, 20);
            this.textBox134.TabIndex = 15;
            // 
            // textBox133
            // 
            this.textBox133.Location = new System.Drawing.Point(290, 75);
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new System.Drawing.Size(225, 20);
            this.textBox133.TabIndex = 14;
            // 
            // textBox132
            // 
            this.textBox132.Location = new System.Drawing.Point(290, 49);
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new System.Drawing.Size(225, 20);
            this.textBox132.TabIndex = 13;
            // 
            // textBox131
            // 
            this.textBox131.Location = new System.Drawing.Point(290, 23);
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new System.Drawing.Size(225, 20);
            this.textBox131.TabIndex = 12;
            // 
            // textBox130
            // 
            this.textBox130.Location = new System.Drawing.Point(19, 257);
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new System.Drawing.Size(225, 20);
            this.textBox130.TabIndex = 11;
            // 
            // textBox129
            // 
            this.textBox129.Location = new System.Drawing.Point(19, 231);
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new System.Drawing.Size(225, 20);
            this.textBox129.TabIndex = 10;
            // 
            // textBox128
            // 
            this.textBox128.Location = new System.Drawing.Point(19, 205);
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new System.Drawing.Size(225, 20);
            this.textBox128.TabIndex = 9;
            // 
            // textBox127
            // 
            this.textBox127.Location = new System.Drawing.Point(19, 179);
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new System.Drawing.Size(225, 20);
            this.textBox127.TabIndex = 8;
            // 
            // textBox126
            // 
            this.textBox126.Location = new System.Drawing.Point(19, 153);
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new System.Drawing.Size(225, 20);
            this.textBox126.TabIndex = 7;
            // 
            // textBox125
            // 
            this.textBox125.Location = new System.Drawing.Point(19, 127);
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new System.Drawing.Size(225, 20);
            this.textBox125.TabIndex = 6;
            // 
            // textBox124
            // 
            this.textBox124.Location = new System.Drawing.Point(19, 101);
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new System.Drawing.Size(225, 20);
            this.textBox124.TabIndex = 5;
            // 
            // textBox123
            // 
            this.textBox123.Location = new System.Drawing.Point(19, 75);
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new System.Drawing.Size(225, 20);
            this.textBox123.TabIndex = 4;
            // 
            // textBox122
            // 
            this.textBox122.Location = new System.Drawing.Point(19, 49);
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new System.Drawing.Size(225, 20);
            this.textBox122.TabIndex = 3;
            // 
            // textBox121
            // 
            this.textBox121.Location = new System.Drawing.Point(19, 23);
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new System.Drawing.Size(225, 20);
            this.textBox121.TabIndex = 2;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 397);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.ShowInTaskbar = false;
            this.Text = "Advance Settings";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Form2_HelpButtonClicked);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.TextBox textBox112;
        private System.Windows.Forms.TextBox textBox111;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TextBox textBox131;
        private System.Windows.Forms.TextBox textBox130;
        private System.Windows.Forms.TextBox textBox129;
        private System.Windows.Forms.TextBox textBox128;
        private System.Windows.Forms.TextBox textBox127;
        private System.Windows.Forms.TextBox textBox126;
        private System.Windows.Forms.TextBox textBox125;
        private System.Windows.Forms.TextBox textBox124;
        private System.Windows.Forms.TextBox textBox123;
        private System.Windows.Forms.TextBox textBox122;
        private System.Windows.Forms.TextBox textBox121;
        private System.Windows.Forms.TextBox textBox140;
        private System.Windows.Forms.TextBox textBox139;
        private System.Windows.Forms.TextBox textBox138;
        private System.Windows.Forms.TextBox textBox137;
        private System.Windows.Forms.TextBox textBox136;
        private System.Windows.Forms.TextBox textBox135;
        private System.Windows.Forms.TextBox textBox134;
        private System.Windows.Forms.TextBox textBox133;
        private System.Windows.Forms.TextBox textBox132;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}
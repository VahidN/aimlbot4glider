﻿using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using XihSolutions.DotMSN;
using XihSolutions.DotMSN.Core;
using XihSolutions.DotMSN.DataTransfer;
using System.Threading;
using AIML;
using Microsoft.Win32;
using Glider.Common.Objects;
using Org.Mentalis.Utilities;

namespace AIML
{
    public partial class Form6 : Form
    {
        private XihSolutions.DotMSN.Messenger messenger = new Messenger();
        private delegate void SetStatusDelegate(string status);
        bool MSNmonior = false;
        Conversation conversation;
        string InstallPath = "";
        GSpellTimer LoginTimer = new GSpellTimer(30*1000, false);
        Contact remoteContact;
        int Unexpected = 1;

        public Form6()
        {
            InitializeComponent();


            // by default this example will emulate the official microsoft windows messenger client
            messenger.Credentials.ClientID = "msmsgs@msnmsgr.com";
            messenger.Credentials.ClientCode = "Q1P7W2E4J9R8U3S5";

            // uncomment this to enable verbose output for debugging
            Settings.TraceSwitch.Level = System.Diagnostics.TraceLevel.Verbose;

            // set the events that we will handle
            // remember that the nameserver is the server that sends contact lists, notifies you of contact status changes, etc.
            // a switchboard server handles the individual conversation sessions.
            messenger.NameserverProcessor.ConnectionEstablished += new EventHandler(NameserverProcessor_ConnectionEstablished);
            messenger.Nameserver.SignedIn += new EventHandler(Nameserver_SignedIn);
            messenger.Nameserver.SignedOff += new SignedOffEventHandler(Nameserver_SignedOff);
            messenger.NameserverProcessor.ConnectingException += new ProcessorExceptionEventHandler(NameserverProcessor_ConnectingException);
            messenger.Nameserver.ExceptionOccurred += new HandlerExceptionEventHandler(Nameserver_ExceptionOccurred);
            messenger.Nameserver.AuthenticationError += new HandlerExceptionEventHandler(Nameserver_AuthenticationError);
            messenger.Nameserver.ServerErrorReceived += new ErrorReceivedEventHandler(Nameserver_ServerErrorReceived);
            messenger.ConversationCreated += new ConversationCreatedEventHandler(messenger_ConversationCreated);
            //messenger.TransferInvitationReceived += new MSNSLPInvitationReceivedEventHandler(messenger_TransferInvitationReceived);            
        }

        private void NameserverProcessor_ConnectionEstablished(object sender, EventArgs e)
        {
            SetStatus("Connected to server");
        }

        private void Nameserver_SignedIn(object sender, EventArgs e)
        {
            SetStatus("Signed into the messenger network as " + messenger.Owner.Name);

            // set our presence status
            messenger.Owner.Status = PresenceStatus.Busy;
            messenger.Owner.Name = "AIMLbot4Glider";
            messenger.Owner.DisplayImage.Image = pictureBox1.Image;
            messenger.Owner.BroadcastDisplayImage();
            
            LoginTimer.Reset();
            
            Invoke(new UpdateContactlistDelegate(UpdateContactlist));

            conversation = messenger.CreateConversation();
        }

        private delegate void UpdateContactlistDelegate();

        private void UpdateContactlist()
        {
            if (messenger.Connected == false)
                return;

            comboBox1.Enabled = true;
            comboBox1.Items.Clear();

            foreach (Contact contact in messenger.ContactList.All)
            {
                if(contact.Mail.ToLower() == comboBox1.Text.ToLower()) remoteContact = contact;
                comboBox1.Items.Add(contact.Name);
            }
        }

        private void Nameserver_SignedOff(object sender, SignedOffEventArgs e)
        {
            SetStatus("Signed off from the messenger network");
            comboBox1.Enabled = false;
            comboBox1.Items.Clear();
            GContext.Main.SetConfigValue("AIMLBot.RemEmail", comboBox1.Text, true);

            if (LoginTimer.IsReady)
            {
                if (Unexpected >= 11)
                {
                    SetStatus("Giving up on re-logins");
                }
                SetStatus("Unexpected signoff, trying to login again in " + 1 * Unexpected + " minutes");
                Thread.Sleep(60 * 1000 * Unexpected);
                Unexpected++;
                Login();
            }
        }

        private void NameserverProcessor_ConnectingException(object sender, ExceptionEventArgs e)
        {
            textBox3.AppendText(e.Exception.ToString() + "Connecting exception" + Environment.NewLine);
            SetStatus("Connecting failed");
        }

        private void Nameserver_AuthenticationError(object sender, ExceptionEventArgs e)
        {
            textBox3.AppendText("Authentication failed, check your account or password. " + "Authentication failed" + Environment.NewLine);
            SetStatus("Authentication failed");
        }

        private void Nameserver_ServerErrorReceived(object sender, MSNErrorEventArgs e)
        {
            // when the MSN server sends an error code we want to be notified.
            textBox3.AppendText(e.MSNError.ToString() + "Server error received" + Environment.NewLine);
            SetStatus("Server error received");
        }

        private void Nameserver_ExceptionOccurred(object sender, ExceptionEventArgs e)
        {
            // ignore the unauthorized exception, since we're handling that error in another method.
            if (e.Exception is UnauthorizedException)
                return;

            textBox3.AppendText(e.Exception.ToString() + Environment.NewLine);
        }

        private void SetStatus(string status)
        {
            this.Invoke(new SetStatusDelegate(SetStatusSynchronized), new object[] { status });
        }

        private void SetStatusSynchronized(string status)
        {
            toolStripStatusLabel1.Text = status;
        }

        private void messenger_ConversationCreated(object sender, ConversationCreatedEventArgs e)
        {
            if (e.Initiator == null)
            {
                conversation = e.Conversation;
                messenger_StartConversation();
            }
        }

        public void messenger_StartConversation()
		{
            conversation.Switchboard.TextMessageReceived += new TextMessageReceivedEventHandler(Switchboard_TextMessageReceived);
            conversation.Switchboard.SessionClosed += new SBChangedEventHandler(Switchboard_SessionClosed);
            //conversation.Switchboard.ContactJoined += new ContactChangedEventHandler(Switchboard_ContactJoined);
            //conversation.Switchboard.ContactLeft += new ContactChangedEventHandler(Switchboard_ContactLeft);		
		}

        private void Switchboard_SessionClosed(object sender, EventArgs e)
        {
            SetStatus("Idle");
        }
        // --------------- Send and get functions ---------------------

        private void Switchboard_TextMessageReceived(object sender, TextMessageEventArgs e)
        {
            // textBox3.AppendText("debug FROM : " + e.Sender.Name + " MESSAGE : " + e.Message.Text + Environment.NewLine);

            if (e.Sender.Mail == remoteContact.Mail)
            {
                if (e.Message.Text[0] != '#')
                {
                    if (GContext.Main.IsAttached)
                    {
                        if (ChatBot.ManualChatWith.Length >= 1)
                        {
                            ChatBot.AIMLBotChatLog("To [" + ChatBot.ManualChatWith + "] : " + e.Message.Text + " (Manual-MSN)");
                            ChatBot.CopyPasteLine(ChatBot.WhisperCommand + ChatBot.ManualChatWith + " " + e.Message.Text);
                        }
                        else
                        {
                            ChatBot.CopyPasteLine(e.Message.Text);
                        }
                    }
                    else
                    {
                        SendString("You’re not attached to wow at the moment", Color.Red);
                        ChatBot.ManualChatWith = "";
                    }
                }
                else
                {
                    switch (e.Message.Text.ToLower())
                    {
                        case "#help":
                            SendString("What you type will be pased into wow directly example : /g hello" + Environment.NewLine
                                + "wil say \'hello\' to your guild", Color.Chocolate);
                            SendString("You can send commands with #xxxx, some examples :" + Environment.NewLine
                                       + "#start, #stop will start and stop the glide" + Environment.NewLine
                                       + "#manual, #manual off will set a manual chat", Color.Chocolate);
                            SendString("#status will give you your current status (mana/health/etc.)" + Environment.NewLine
                                       + "#monitor, #monitor off will set AIMLbot4Glider to send you all chat activeties.", Color.Chocolate);
                            SendString("For more commands see the AIMLbot4Glider help file, to get it type #gethelp", Color.Chocolate);
                            break;
                        case "#manual on":
                        case "#manual":
                            if (ChatBot.LastFrom.Length >= 1)
                            {
                                if (ChatBot.ManualChatWith.Length <= 1)
                                {
                                    ChatBot.ManualChatWith = ChatBot.LastFrom;
                                    SendString("Manual chat is now on for " + ChatBot.ManualChatWith);
                                }
                                else SendString("Manual chat already on for " + ChatBot.ManualChatWith, Color.Red);
                            }
                            else SendString("No resent chat to take control over", Color.Red);
                            break;
                        case "#manual off":
                            if (ChatBot.ManualChatWith.Length >= 1)
                            {
                                ChatBot.ManualChatWith = "";
                                SendString("Manual chat is now off");
                            }
                            else SendString("No manual chat is active", Color.Red);
                            break;
                        case "#blacklist":
                            SendString("Blacklist incidents: " + ChatBot.BlacklistCount + "/" + ChatBot.MaxBlacklistCount);

                            string tmp = "";

                            foreach (string mesg in ChatBot.BlackList)
                            {
                                tmp = tmp + mesg + ", ";
                            }

                            SendString("Player names on the blacklist: " + tmp.Substring(0 , tmp.Length - 2));

                            break;
                        case "#deaths":
                            SendString("You died: " + ChatBot.DeadCount + " times since the start of AIMLbot4Glider");
                            break;
                        case "#bgs":
                            SendString("You have joined: " + ChatBot.BGCount + " battlegrounds since the start of AIMLbot4Glider");
                            break;
                        case "#reset":
                            ChatBot.BlacklistCount = 0;
                            SendString("Blacklist count has been resetted");
                            break;
                        case "#who":
                            if (GContext.Main.IsAttached)
                            {
                                SendString(GContext.Main.Me.Name + " the level " + GContext.Main.Me.Level + " " + GContext.Main.Me.PlayerRace.ToString() + " " + GContext.Main.Me.PlayerClass.ToString());
                            if (GContext.Main.Me.Level != 70)
                               SendString(Math.Round((System.Convert.ToDouble(GContext.Main.Me.Experience) / System.Convert.ToDouble(GContext.Main.Me.NextLevelExperience)) * 100) + "% of this level done");
                            }
                            else SendString("Glider is not attached");
                            break;
                        case "#attached":
                            if (GContext.Main.IsAttached)
                            {
                                SendString("Glider is attached");
                            } else SendString("Glider is not attached");                         
                            break;                     
                        case "#status":
                            if (GContext.Main.IsAttached)
                            {
                                if (GContext.Main.Me.PlayerClass.ToString() == "Rogue")
                                {
                                    SendString("Current health at " + Math.Round(GContext.Main.Me.Health * 100) + "%" + Environment.NewLine +
                                    "Current energy is " + GContext.Main.Me.Energy);
                                }

                                if (GContext.Main.Me.PlayerClass.ToString() == "Warrior")
                                {
                                    SendString("Current health at " + Math.Round(GContext.Main.Me.Health * 100) + "%" + Environment.NewLine +
                                    "Current rage is " + GContext.Main.Me.Rage);
                                }

                                if (GContext.Main.Me.PlayerClass.ToString() != "Rogue" && GContext.Main.Me.PlayerClass.ToString() != "Warrior")
                                {
                                    SendString("Current health at " + Math.Round(GContext.Main.Me.Health * 100) + "%" + Environment.NewLine +
                                    "Current mana at " + Math.Round(GContext.Main.Me.Mana * 100) + "%");
                                }

                                if (GContext.Main.Me.AmmoCount >= 1) SendString("We have " + GContext.Main.Me.AmmoCount + " ammo left");

                                if (GContext.Main.Me.IsDead) SendString("Your dead");
                                else if (GContext.Main.Me.Target.Name.Length >= 1) SendString("Your targeting : " + GContext.Main.Me.Target.Name);
                            }
                            else SendString("Glider is not attached", Color.Red);
                            break;
                        case "#kills":
                            SendString("Kills : " + ChatBot.KillCount + " since the start of AIMLbot4Glider");
                            break;
                        case "#where":
                            if (GContext.Main.IsAttached)
                            {
                                SendString("Your zone: " + GContext.Main.ZoneText + Environment.NewLine
                                    + "Your sub-zone: " + GContext.Main.SubZoneText);
                            }
                            else SendString("Glider is not attached", Color.Red);
                            break;
                        case "#stop":
                            if (GContext.Main.IsGliding)
                            {
                                conversation.Switchboard.SendTypingMessage();
                                ChatBot.OutOfCombatWait(2000);
                                GContext.Main.KillAction("AIMLbot4Glider", false);
                                SendString("Glide stopped");
                            }
                            else SendString("Cant stop Glider, your not gliding at the moment", Color.Red);
                            break;
                        case "#start":
                            if (GContext.Main.IsAttached)
                            {
                                if (!GContext.Main.IsGliding && GContext.Main.IsAttached)
                                {
                                    conversation.Switchboard.SendTypingMessage();
                                    ChatBot.OutOfCombatWait(2000);
                                    GContext.Main.StartGlide();
                                    SendString("Glider started");
                                }
                                else
                                {
                                    if (GContext.Main.IsGliding) SendString("Cant start glide, you're already gliding", Color.Red);
                                    if (!GContext.Main.IsAttached) SendString("Cant start glide, Glider is not attached", Color.Red);
                                }
                            }
                            else SendString("Glider is not attached.", Color.Red);
                            break;
                        case "#last":
                                SendString("Last message was from: " + ChatBot.LastFrom);
                            break;
                        case "#monitor on":
                        case "#monitor":
                            MSNmonior = true;
                            SendString("Active MSN monitor is now on");
                            break;
                        case "#monitor off":
                            MSNmonior = false;
                            SendString("Active MSN monitor is now off");
                            break;
                        case "#exit":
                        case "#quit":
                            if (GContext.Main.IsAttached)
                            {
                                conversation.Switchboard.SendTypingMessage();
                                ChatBot.OutOfCombatWait(2000);
                                GContext.Main.KillAction("AIMLbot4Glider", false);
                                ChatBot.CopyPasteLine("/quit");
                                SendString("You quit WoW.");
                            }
                            else SendString("Glider is not attached", Color.Red);
                            break;
                        case "#version":
                            SendString("AIMLbot4Glider : " + ChatBot.Version + Environment.NewLine +
                                        "Glider : " + GContext.Main.Version);
                            break;
                        case "#getlog":
                            MSNSLPHandler msnslpHandler = messenger.GetMSNSLPHandler(e.Sender.Mail);
                            FileStream fileStream = new FileStream("Classes\\AIMLbot4Glider\\AIMLbot.log", FileMode.Open, FileAccess.Read, FileShare.Read);
                            P2PTransferSession session = msnslpHandler.SendInvitation(messenger.Owner.Mail, e.Sender.Mail, Path.GetFileName("Classes\\AIMLbot4Glider\\AIMLbot.log"), fileStream);
                            break;
                        case "#getgliderlog":
                            MSNSLPHandler msnslpHandler4 = messenger.GetMSNSLPHandler(e.Sender.Mail);
                            FileStream fileStream4 = new FileStream("Glider.log", FileMode.Open, FileAccess.Read, FileShare.Read);
                            P2PTransferSession session4 = msnslpHandler4.SendInvitation(messenger.Owner.Mail, e.Sender.Mail, Path.GetFileName("Glider.log"), fileStream4);
                            break;
                        case "#gethelp":
                            MSNSLPHandler msnslpHandler2 = messenger.GetMSNSLPHandler(e.Sender.Mail);
                            FileStream fileStream2 = new FileStream("Classes\\AIMLbot4Glider\\aimlbot help.chm", FileMode.Open, FileAccess.Read, FileShare.Read);
                            P2PTransferSession session2 = msnslpHandler2.SendInvitation(messenger.Owner.Mail, e.Sender.Mail, Path.GetFileName("Classes\\AIMLbot4Glider\\aimlbot help.chm"), fileStream2);
                            break;
                        case "#getscreen":
                            if (GContext.Main.IsAttached)
                            {
                                string filename = MakeScreenshot();

                                MSNSLPHandler msnslpHandler3 = messenger.GetMSNSLPHandler(e.Sender.Mail);
                                FileStream fileStream3 = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                                P2PTransferSession session3 = msnslpHandler3.SendInvitation(messenger.Owner.Mail, e.Sender.Mail, Path.GetFileName(filename), fileStream3);
                            } else SendString("Glider is not attached", Color.Red);
                            break;
                        case "#guild":
                        case "#guild on":
                            ChatBot.MSNguild = true;
                            SendString("Guild chat monitor is now active.");
                            break;
                        case "#shutdown":
                            ChatBot.MSNguild = true;
                            WindowsController.ExitWindows(RestartOptions.PowerOff, true);
                            SendString("The computer will now try to close windows and powerdown.");
                            //ShutDown();
                            
                        break;
                        case "#guild off":
                            ChatBot.MSNguild = false;
                            SendString("Guild chat monitor is now inactive.");
                            break;
                        case "#bg":
                        case "#bg on":
                            ChatBot.MSNbg = true;
                            SendString("Battleground chat monitor is now active.");
                            break;
                        case "#bg off":
                            ChatBot.MSNbg = false;
                            SendString("Battleground chat monitor is now inactive.");
                            break;
                        case "#all":
                        case "#all on":
                            ChatBot.MSNbg = true;
                            ChatBot.MSNguild = true;
                            MSNmonior = true;
                            SendString("All monitors are now active.");
                            break;
                        case "#all off":
                            ChatBot.MSNbg = false;
                            ChatBot.MSNguild = false;
                            MSNmonior = false;
                            SendString("All monitors are now inactive");
                            break;
                        case "#aiml":
                        case "#aiml off":
                            ChatBot.AIMLautoanswer = false;
                            SendString("AIML answers are temporarily deactivated");
                            break;
                        case "#aiml on":
                            ChatBot.AIMLautoanswer = true;
                            SendString("AIML answers are active");
                            break;
                        default:
                            SendString(e.Message.Text + " is not a known command, type #help for some examples.", Color.Red);
                            break;
                    }
                }
            } //else SendString("You have no privileges to speak to me...", Color.Red); // Disabled to avoid issues with dual gliding.
        }

        private void SendString(string sendtext)
        {
            SendString(sendtext, Color.DarkBlue);
        }

        private void SendString(string sendtext, Color sendcolor) 
        {

            if (conversation.SwitchboardProcessor.Connected == false)
            {
                conversation.Messenger.Nameserver.RequestSwitchboard(conversation.Switchboard, this);
            }

            if (conversation.Switchboard.Contacts.Count == 0)
            {
                //conversation.Messenger.Nameserver.RequestSwitchboard(conversation.Switchboard, this);

                conversation = messenger.CreateConversation();

                int x = 0;

                while (conversation.Switchboard.Contacts.Count == 0 && x <= 10)
                {
                    conversation.Invite(remoteContact);
                    Thread.Sleep(500);
                    x++;
                }

                if (x >= 10) SetStatus("Conversation Time out.");
                else messenger_StartConversation();
                
            }

            if (messenger.Connected && remoteContact.Online)
            {
                try
                {
                    conversation.Switchboard.SendTypingMessage();
                    TextMessage message = new TextMessage(sendtext);
                    message.Color = sendcolor;
                    conversation.Switchboard.SendTextMessage(message);
                    SetStatus("Conversation Active (" + remoteContact.Name + ")");
                }
                catch (Exception e)
                {
                    SetStatus("Critical MSN error: " + e.Message);
                    ChatBot.AIMLBOTLog("Detailed MSN error: " + e.ToString());
                }
            }
            else
            {
                SetStatus("Can’t send a message, remote contact is not active");
                ChatBot.MSNbg = false;
                ChatBot.MSNguild = false;
                MSNmonior = false;
                ChatBot.AIMLautoanswer = true;
                ChatBot.ManualChatWith = "";
            }
        }

        // ------------------------ End ---------------------

        private void Login()
        {
            if (messenger.Connected)
            {
                SetStatus("Disconnecting from server");
                button1.Text = "Connect";
                messenger.Disconnect();
            }
            else
            {

                messenger.Credentials.Account = textBox1.Text;
                messenger.Credentials.Password = textBox2.Text;

                SetStatus("Connecting to server");
                button1.Text = "Disconnect";
                messenger.Connect();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginTimer.Reset();
            Unexpected = 1;

            Login();

            GContext.Main.SetConfigValue("AIMLBot.AutoMSN", CheckboxToString(checkBox1.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.WLEmail", textBox1.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.WLPass", textBox2.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.RemEmail", comboBox1.Text, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SendString("Hello world", Color.Chocolate);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //textBox3.AppendText("Check");
            if (messenger.Connected) // && MyContact != null)
            {
                if (ChatBot.ManualMessage.Length >= 1)
                {
                    textBox3.AppendText("From [" + ChatBot.ManualChatWith + "] : " + ChatBot.ManualMessage + " (Manual)");
                    SendString("From [" + ChatBot.ManualChatWith + "] : " + ChatBot.ManualMessage, Color.Green);
                    ChatBot.ManualMessage = "";
                }

                if (ChatBot.MSNmessage.Length >= 1)
                {
                    if (MSNmonior) SendString(ChatBot.MSNmessage, Color.Gray);
                    ChatBot.MSNmessage = "";
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GContext.Main.SetConfigValue("AIMLBot.WLEmail", textBox1.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.WLPass", textBox2.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.RemEmail", comboBox1.Text, true);
            SetStatus("Disconnecting from server");
            messenger.Disconnect();
            //this.Hide();
            this.Visible = false;
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            textBox1.Text = GContext.Main.GetConfigString("AIMLBot.WLEmail");
            textBox2.Text = GContext.Main.GetConfigString("AIMLBot.WLPass");
            comboBox1.Text = GContext.Main.GetConfigString("AIMLBot.RemEmail");
            checkBox1.Checked = GContext.Main.GetConfigBool("AIMLBot.AutoMSN");

            //Path : HKEY_LOCAL_MACHINE\SOFTWARE\Blizzard Entertainment\World of Warcraft
            //Key : InstallPath

            RegistryKey regKeyAppRoot = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Blizzard Entertainment\\World of Warcraft");
            InstallPath = (string)regKeyAppRoot.GetValue("InstallPath");

            if (textBox2.Text.Length >= 1 && checkBox1.Checked)
            {
                Login();
            }
        }

        private string MakeScreenshot()
        {
            string Newest_File = null;
            ChatBot.OutOfCombatWait(2000);
            ChatBot.CopyPasteLine("/script Screenshot()");
            Thread.Sleep(10000);

            DateTime tmp = new DateTime(0);

            DirectoryInfo di = new DirectoryInfo(InstallPath + "Screenshots");
            FileInfo[] rgFiles = di.GetFiles("*.jpg");

            foreach (FileInfo fi in rgFiles)
            {
                if (tmp < fi.CreationTime)
                {
                    tmp = fi.CreationTime;
                    Newest_File = fi.Name.ToString();
                }                   
            }

            return InstallPath + "Screenshots\\" + Newest_File;
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void comboBox1_ValueMemberChanged(object sender, EventArgs e)
        {
            Invoke(new UpdateContactlistDelegate(UpdateContactlist));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void Form6_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        string CheckboxToString(bool tmp)
        {
            if (tmp) return "True"; else return "False";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            GContext.Main.Log(MakeScreenshot());
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            GContext.Main.Log("DEBUG MSN contact : " + remoteContact.Name);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (messenger.Connected == false)
                return;

            foreach (Contact contact in messenger.ContactList.All)
            {
                if (contact.Mail.ToLower() == comboBox1.Text.ToLower()) remoteContact = contact;
            }

            remoteContact.OnForwardList = true;
            remoteContact.OnAllowedList = true;
        }

        private void Form6_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }

    }
}

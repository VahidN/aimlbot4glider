﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Glider.Common.Objects;

namespace AIML
{
    public partial class SendLine : Form
    {
        public SendLine()
        {
            InitializeComponent();
        }

        void DoSendNow()
        {
            if (GContext.Main.IsAttached)
            {
                ChatBot.CopyPasteLine(textBox1.Text);
                textBox1.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoSendNow();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                DoSendNow();
            }
        }

        private void SendLine_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }
    }
}

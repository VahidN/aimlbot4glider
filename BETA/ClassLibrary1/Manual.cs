﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Glider.Common.Objects;

namespace AIML
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        void DoSendNow()
        {
            textBox1.AppendText("To [" + ChatBot.ManualChatWith + "] : " + textBox2.Text + Environment.NewLine);
            ChatBot.AIMLBotChatLog("To [" + ChatBot.ManualChatWith + "] : " + textBox2.Text + " (Manual)");
            ChatBot.CopyPasteLine(ChatBot.WhisperCommand + ChatBot.ManualChatWith + " " + textBox2.Text);
            textBox2.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoSendNow();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ChatBot.LastFrom = ChatBot.ManualChatWith;
            ChatBot.ManualChatWith = "";
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ChatBot.ManualMessage.Length >= 1)
            {
                textBox1.AppendText("From [" + ChatBot.ManualChatWith + "] : " + ChatBot.ManualMessage + Environment.NewLine);
                ChatBot.ManualMessage = "";
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                DoSendNow();
            }
        }

        private void Form5_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}

//!Reference: AIMLbot4Glider.dll
//!Class: Glider.Common.Objects.AIMLbot4Glider

using System;
using Glider.Common.Objects;
using AIML;

namespace Glider.Common.Objects
{
    public class AIMLbot4Glider : GGameClass
    {
        public override bool IsSelectable { get { return false; } }
        public override string DisplayName { get { return "AMLbot4Glider"; } }

        public override void Startup()
        {
            ChatBot.Loader();
        }

        public override void Shutdown()
        {
            ChatBot.UnLoader();
        }

        public override void OnStartGlide()                     // Not working as this class is never set in glider options
        {
            ChatBot.AIMLBOTLog("Never use AIMLbot4Glider as your class to run the glide !!", false);
            GContext.Main.KillAction("AIMLbot4Glider", false);
        }       
    }
}

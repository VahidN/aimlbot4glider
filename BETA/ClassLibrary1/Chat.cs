﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AIML
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        void DoChatNow()
        {
            string answer = ChatBot.MakeAnswer("Default", textBox2.Text);
            textBox1.AppendText("You : " + textBox2.Text + Environment.NewLine);

            if (answer.Length <= 1) answer = ChatBot.IdontKnow() + " (Default)";

            textBox1.AppendText("AIML: " + answer + Environment.NewLine);
            textBox2.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DoChatNow();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                DoChatNow();
            }
        }

        private void Form4_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }
    }
}

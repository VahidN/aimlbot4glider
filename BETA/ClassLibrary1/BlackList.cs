﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AIML
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }   

        private void Form3_Load(object sender, EventArgs e)
        {
            foreach (string mesg in ChatBot.BlackList)
            {
                textBox1.AppendText(mesg + Environment.NewLine);
                listBox1.Items.Add(mesg);
            }
            listBox1.SetSelected(0, true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void List2Text()
        {
            textBox1.Clear();

            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                textBox1.AppendText(listBox1.Items[i].ToString() + Environment.NewLine);  
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List2Text();

            string boxtext = textBox1.Text;
            string[] split = boxtext.Split(new char[] { '\n' });

            ChatBot.BlackList.Clear();

            foreach (string mesg in split)
            {
                if (mesg.Length >= 1) ChatBot.BlackList.Add(mesg);
            }

            this.Hide();
        }

        private void Form3_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Length >= 1)
            {
                listBox1.Items.Add(textBox2.Text);
                textBox2.Text = "";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
            listBox1.SetSelected(0, true);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            List2Text();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Glider.Common.Objects;

namespace AIML
{
    public partial class Form2 : Form
    {

        public Form2()
        {
            InitializeComponent();
        }

        public string NoEmptyforSave(string Input)
        {
            string tmp = Input;
            if (tmp.Length <= 0) tmp = ChatBot.EmplyReplacement;
            return tmp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GContext.Main.SetConfigValue("AIMLBot.Appriciation1", NoEmptyforSave(textBox1.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation2", NoEmptyforSave(textBox2.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation3", NoEmptyforSave(textBox3.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation4", NoEmptyforSave(textBox4.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation5", NoEmptyforSave(textBox5.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation6", NoEmptyforSave(textBox6.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation7", NoEmptyforSave(textBox7.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation8", NoEmptyforSave(textBox8.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation9", NoEmptyforSave(textBox9.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation10", NoEmptyforSave(textBox10.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation11", NoEmptyforSave(textBox11.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation12", NoEmptyforSave(textBox12.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation13", NoEmptyforSave(textBox13.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation14", NoEmptyforSave(textBox14.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation15", NoEmptyforSave(textBox15.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation16", NoEmptyforSave(textBox16.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation17", NoEmptyforSave(textBox17.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation18", NoEmptyforSave(textBox18.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation19", NoEmptyforSave(textBox19.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Appriciation20", NoEmptyforSave(textBox20.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.Correction1", NoEmptyforSave(textBox21.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction2", NoEmptyforSave(textBox22.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction3", NoEmptyforSave(textBox23.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction4", NoEmptyforSave(textBox24.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction5", NoEmptyforSave(textBox25.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction6", NoEmptyforSave(textBox26.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction7", NoEmptyforSave(textBox27.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction8", NoEmptyforSave(textBox28.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction9", NoEmptyforSave(textBox29.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction10", NoEmptyforSave(textBox30.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction11", NoEmptyforSave(textBox31.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction12", NoEmptyforSave(textBox32.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction13", NoEmptyforSave(textBox33.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction14", NoEmptyforSave(textBox34.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction15", NoEmptyforSave(textBox35.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction16", NoEmptyforSave(textBox36.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction17", NoEmptyforSave(textBox37.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction18", NoEmptyforSave(textBox38.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction19", NoEmptyforSave(textBox39.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Correction20", NoEmptyforSave(textBox40.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.Invite1", NoEmptyforSave(textBox41.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite2", NoEmptyforSave(textBox42.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite3", NoEmptyforSave(textBox43.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite4", NoEmptyforSave(textBox44.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite5", NoEmptyforSave(textBox45.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite6", NoEmptyforSave(textBox46.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite7", NoEmptyforSave(textBox47.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite8", NoEmptyforSave(textBox48.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite9", NoEmptyforSave(textBox49.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite10", NoEmptyforSave(textBox50.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite11", NoEmptyforSave(textBox51.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite12", NoEmptyforSave(textBox52.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite13", NoEmptyforSave(textBox53.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite14", NoEmptyforSave(textBox54.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite15", NoEmptyforSave(textBox55.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite16", NoEmptyforSave(textBox56.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite17", NoEmptyforSave(textBox57.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite18", NoEmptyforSave(textBox58.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite19", NoEmptyforSave(textBox59.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Invite20", NoEmptyforSave(textBox60.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.Opening1", NoEmptyforSave(textBox61.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening2", NoEmptyforSave(textBox62.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening3", NoEmptyforSave(textBox63.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening4", NoEmptyforSave(textBox64.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening5", NoEmptyforSave(textBox65.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening6", NoEmptyforSave(textBox66.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening7", NoEmptyforSave(textBox67.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening8", NoEmptyforSave(textBox68.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening9", NoEmptyforSave(textBox69.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening10", NoEmptyforSave(textBox70.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening11", NoEmptyforSave(textBox71.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening12", NoEmptyforSave(textBox72.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening13", NoEmptyforSave(textBox73.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening14", NoEmptyforSave(textBox74.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening15", NoEmptyforSave(textBox75.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening16", NoEmptyforSave(textBox76.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening17", NoEmptyforSave(textBox77.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening18", NoEmptyforSave(textBox78.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening19", NoEmptyforSave(textBox79.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Opening20", NoEmptyforSave(textBox80.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.BG1", NoEmptyforSave(textBox81.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG2", NoEmptyforSave(textBox82.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG3", NoEmptyforSave(textBox83.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG4", NoEmptyforSave(textBox84.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG5", NoEmptyforSave(textBox85.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG6", NoEmptyforSave(textBox86.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG7", NoEmptyforSave(textBox87.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG8", NoEmptyforSave(textBox88.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG9", NoEmptyforSave(textBox89.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG10", NoEmptyforSave(textBox90.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG11", NoEmptyforSave(textBox91.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG12", NoEmptyforSave(textBox92.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG13", NoEmptyforSave(textBox93.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG14", NoEmptyforSave(textBox94.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG15", NoEmptyforSave(textBox95.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG16", NoEmptyforSave(textBox96.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG17", NoEmptyforSave(textBox97.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG18", NoEmptyforSave(textBox98.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG19", NoEmptyforSave(textBox99.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BG20", NoEmptyforSave(textBox100.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.BGpos1", NoEmptyforSave(textBox101.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos2", NoEmptyforSave(textBox102.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos3", NoEmptyforSave(textBox103.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos4", NoEmptyforSave(textBox104.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos5", NoEmptyforSave(textBox105.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos6", NoEmptyforSave(textBox106.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos7", NoEmptyforSave(textBox107.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos8", NoEmptyforSave(textBox108.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos9", NoEmptyforSave(textBox109.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGpos10", NoEmptyforSave(textBox110.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.BGneg1", NoEmptyforSave(textBox111.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg2", NoEmptyforSave(textBox112.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg3", NoEmptyforSave(textBox113.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg4", NoEmptyforSave(textBox114.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg5", NoEmptyforSave(textBox115.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg6", NoEmptyforSave(textBox116.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg7", NoEmptyforSave(textBox117.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg8", NoEmptyforSave(textBox118.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg9", NoEmptyforSave(textBox119.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.BGneg10", NoEmptyforSave(textBox120.Text), true);

            GContext.Main.SetConfigValue("AIMLBot.Duel1", NoEmptyforSave(textBox121.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel2", NoEmptyforSave(textBox122.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel3", NoEmptyforSave(textBox123.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel4", NoEmptyforSave(textBox124.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel5", NoEmptyforSave(textBox125.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel6", NoEmptyforSave(textBox126.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel7", NoEmptyforSave(textBox127.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel8", NoEmptyforSave(textBox128.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel9", NoEmptyforSave(textBox129.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel10", NoEmptyforSave(textBox130.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel11", NoEmptyforSave(textBox131.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel12", NoEmptyforSave(textBox132.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel13", NoEmptyforSave(textBox133.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel14", NoEmptyforSave(textBox134.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel15", NoEmptyforSave(textBox135.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel16", NoEmptyforSave(textBox136.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel17", NoEmptyforSave(textBox137.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel18", NoEmptyforSave(textBox138.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel19", NoEmptyforSave(textBox139.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.Duel20", NoEmptyforSave(textBox140.Text), true);

            ChatBot.GetSettings();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private string GetEmptyStrings(string name)
        {
            string tmp = GContext.Main.GetConfigString(name);
            if (tmp == ChatBot.EmplyReplacement) tmp = "";
            return tmp;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            textBox1.Text = GetEmptyStrings("AIMLBot.Appriciation1");
            textBox2.Text = GetEmptyStrings("AIMLBot.Appriciation2");
            textBox3.Text = GetEmptyStrings("AIMLBot.Appriciation3");
            textBox4.Text = GetEmptyStrings("AIMLBot.Appriciation4");
            textBox5.Text = GetEmptyStrings("AIMLBot.Appriciation5");
            textBox6.Text = GetEmptyStrings("AIMLBot.Appriciation6");
            textBox7.Text = GetEmptyStrings("AIMLBot.Appriciation7");
            textBox8.Text = GetEmptyStrings("AIMLBot.Appriciation8");
            textBox9.Text = GetEmptyStrings("AIMLBot.Appriciation9");
            textBox10.Text = GetEmptyStrings("AIMLBot.Appriciation10");
            textBox11.Text = GetEmptyStrings("AIMLBot.Appriciation11");
            textBox12.Text = GetEmptyStrings("AIMLBot.Appriciation12");
            textBox13.Text = GetEmptyStrings("AIMLBot.Appriciation13");
            textBox14.Text = GetEmptyStrings("AIMLBot.Appriciation14");
            textBox15.Text = GetEmptyStrings("AIMLBot.Appriciation15");
            textBox16.Text = GetEmptyStrings("AIMLBot.Appriciation16");
            textBox17.Text = GetEmptyStrings("AIMLBot.Appriciation17");
            textBox18.Text = GetEmptyStrings("AIMLBot.Appriciation18");
            textBox19.Text = GetEmptyStrings("AIMLBot.Appriciation19");
            textBox20.Text = GetEmptyStrings("AIMLBot.Appriciation20");

            textBox21.Text = GetEmptyStrings("AIMLBot.Correction1");
            textBox22.Text = GetEmptyStrings("AIMLBot.Correction2");
            textBox23.Text = GetEmptyStrings("AIMLBot.Correction3");
            textBox24.Text = GetEmptyStrings("AIMLBot.Correction4");
            textBox25.Text = GetEmptyStrings("AIMLBot.Correction5");
            textBox26.Text = GetEmptyStrings("AIMLBot.Correction6");
            textBox27.Text = GetEmptyStrings("AIMLBot.Correction7");
            textBox28.Text = GetEmptyStrings("AIMLBot.Correction8");
            textBox29.Text = GetEmptyStrings("AIMLBot.Correction9");
            textBox30.Text = GetEmptyStrings("AIMLBot.Correction10");
            textBox31.Text = GetEmptyStrings("AIMLBot.Correction11");
            textBox32.Text = GetEmptyStrings("AIMLBot.Correction12");
            textBox33.Text = GetEmptyStrings("AIMLBot.Correction13");
            textBox34.Text = GetEmptyStrings("AIMLBot.Correction14");
            textBox35.Text = GetEmptyStrings("AIMLBot.Correction15");
            textBox36.Text = GetEmptyStrings("AIMLBot.Correction16");
            textBox37.Text = GetEmptyStrings("AIMLBot.Correction17");
            textBox38.Text = GetEmptyStrings("AIMLBot.Correction18");
            textBox39.Text = GetEmptyStrings("AIMLBot.Correction19");
            textBox40.Text = GetEmptyStrings("AIMLBot.Correction20");

            textBox41.Text = GetEmptyStrings("AIMLBot.Invite1");
            textBox42.Text = GetEmptyStrings("AIMLBot.Invite2");
            textBox43.Text = GetEmptyStrings("AIMLBot.Invite3");
            textBox44.Text = GetEmptyStrings("AIMLBot.Invite4");
            textBox45.Text = GetEmptyStrings("AIMLBot.Invite5");
            textBox46.Text = GetEmptyStrings("AIMLBot.Invite6");
            textBox47.Text = GetEmptyStrings("AIMLBot.Invite7");
            textBox48.Text = GetEmptyStrings("AIMLBot.Invite8");
            textBox49.Text = GetEmptyStrings("AIMLBot.Invite9");
            textBox50.Text = GetEmptyStrings("AIMLBot.Invite10");
            textBox51.Text = GetEmptyStrings("AIMLBot.Invite11");
            textBox52.Text = GetEmptyStrings("AIMLBot.Invite12");
            textBox53.Text = GetEmptyStrings("AIMLBot.Invite13");
            textBox54.Text = GetEmptyStrings("AIMLBot.Invite14");
            textBox55.Text = GetEmptyStrings("AIMLBot.Invite15");
            textBox56.Text = GetEmptyStrings("AIMLBot.Invite16");
            textBox57.Text = GetEmptyStrings("AIMLBot.Invite17");
            textBox58.Text = GetEmptyStrings("AIMLBot.Invite18");
            textBox59.Text = GetEmptyStrings("AIMLBot.Invite19");
            textBox60.Text = GetEmptyStrings("AIMLBot.Invite20");

            textBox61.Text = GetEmptyStrings("AIMLBot.Opening1");
            textBox62.Text = GetEmptyStrings("AIMLBot.Opening2");
            textBox63.Text = GetEmptyStrings("AIMLBot.Opening3");
            textBox64.Text = GetEmptyStrings("AIMLBot.Opening4");
            textBox65.Text = GetEmptyStrings("AIMLBot.Opening5");
            textBox66.Text = GetEmptyStrings("AIMLBot.Opening6");
            textBox67.Text = GetEmptyStrings("AIMLBot.Opening7");
            textBox68.Text = GetEmptyStrings("AIMLBot.Opening8");
            textBox69.Text = GetEmptyStrings("AIMLBot.Opening9");
            textBox70.Text = GetEmptyStrings("AIMLBot.Opening10");
            textBox71.Text = GetEmptyStrings("AIMLBot.Opening11");
            textBox72.Text = GetEmptyStrings("AIMLBot.Opening12");
            textBox73.Text = GetEmptyStrings("AIMLBot.Opening13");
            textBox74.Text = GetEmptyStrings("AIMLBot.Opening14");
            textBox75.Text = GetEmptyStrings("AIMLBot.Opening15");
            textBox76.Text = GetEmptyStrings("AIMLBot.Opening16");
            textBox77.Text = GetEmptyStrings("AIMLBot.Opening17");
            textBox78.Text = GetEmptyStrings("AIMLBot.Opening18");
            textBox79.Text = GetEmptyStrings("AIMLBot.Opening19");
            textBox80.Text = GetEmptyStrings("AIMLBot.Opening20");

            textBox81.Text = GetEmptyStrings("AIMLBot.BG1");
            textBox82.Text = GetEmptyStrings("AIMLBot.BG2");
            textBox83.Text = GetEmptyStrings("AIMLBot.BG3");
            textBox84.Text = GetEmptyStrings("AIMLBot.BG4");
            textBox85.Text = GetEmptyStrings("AIMLBot.BG5");
            textBox86.Text = GetEmptyStrings("AIMLBot.BG6");
            textBox87.Text = GetEmptyStrings("AIMLBot.BG7");
            textBox88.Text = GetEmptyStrings("AIMLBot.BG8");
            textBox89.Text = GetEmptyStrings("AIMLBot.BG9");
            textBox90.Text = GetEmptyStrings("AIMLBot.BG10");
            textBox91.Text = GetEmptyStrings("AIMLBot.BG11");
            textBox92.Text = GetEmptyStrings("AIMLBot.BG12");
            textBox93.Text = GetEmptyStrings("AIMLBot.BG13");
            textBox94.Text = GetEmptyStrings("AIMLBot.BG14");
            textBox95.Text = GetEmptyStrings("AIMLBot.BG15");
            textBox96.Text = GetEmptyStrings("AIMLBot.BG16");
            textBox97.Text = GetEmptyStrings("AIMLBot.BG17");
            textBox98.Text = GetEmptyStrings("AIMLBot.BG18");
            textBox99.Text = GetEmptyStrings("AIMLBot.BG19");
            textBox100.Text = GetEmptyStrings("AIMLBot.BG20");

            textBox101.Text = GetEmptyStrings("AIMLBot.BGpos1");
            textBox102.Text = GetEmptyStrings("AIMLBot.BGpos2");
            textBox103.Text = GetEmptyStrings("AIMLBot.BGpos3");
            textBox104.Text = GetEmptyStrings("AIMLBot.BGpos4");
            textBox105.Text = GetEmptyStrings("AIMLBot.BGpos5");
            textBox106.Text = GetEmptyStrings("AIMLBot.BGpos6");
            textBox107.Text = GetEmptyStrings("AIMLBot.BGpos7");
            textBox108.Text = GetEmptyStrings("AIMLBot.BGpos8");
            textBox109.Text = GetEmptyStrings("AIMLBot.BGpos9");
            textBox110.Text = GetEmptyStrings("AIMLBot.BGpos10");

            textBox111.Text = GetEmptyStrings("AIMLBot.BGneg1");
            textBox112.Text = GetEmptyStrings("AIMLBot.BGneg2");
            textBox113.Text = GetEmptyStrings("AIMLBot.BGneg3");
            textBox114.Text = GetEmptyStrings("AIMLBot.BGneg4");
            textBox115.Text = GetEmptyStrings("AIMLBot.BGneg5");
            textBox116.Text = GetEmptyStrings("AIMLBot.BGneg6");
            textBox117.Text = GetEmptyStrings("AIMLBot.BGneg7");
            textBox118.Text = GetEmptyStrings("AIMLBot.BGneg8");
            textBox119.Text = GetEmptyStrings("AIMLBot.BGneg9");
            textBox120.Text = GetEmptyStrings("AIMLBot.BGneg10");

            textBox121.Text = GetEmptyStrings("AIMLBot.Duel1");
            textBox122.Text = GetEmptyStrings("AIMLBot.Duel2");
            textBox123.Text = GetEmptyStrings("AIMLBot.Duel3");
            textBox124.Text = GetEmptyStrings("AIMLBot.Duel4");
            textBox125.Text = GetEmptyStrings("AIMLBot.Duel5");
            textBox126.Text = GetEmptyStrings("AIMLBot.Duel6");
            textBox127.Text = GetEmptyStrings("AIMLBot.Duel7");
            textBox128.Text = GetEmptyStrings("AIMLBot.Duel8");
            textBox129.Text = GetEmptyStrings("AIMLBot.Duel9");
            textBox130.Text = GetEmptyStrings("AIMLBot.Duel10");
            textBox131.Text = GetEmptyStrings("AIMLBot.Duel11");
            textBox132.Text = GetEmptyStrings("AIMLBot.Duel12");
            textBox133.Text = GetEmptyStrings("AIMLBot.Duel13");
            textBox134.Text = GetEmptyStrings("AIMLBot.Duel14");
            textBox135.Text = GetEmptyStrings("AIMLBot.Duel15");
            textBox136.Text = GetEmptyStrings("AIMLBot.Duel16");
            textBox137.Text = GetEmptyStrings("AIMLBot.Duel17");
            textBox138.Text = GetEmptyStrings("AIMLBot.Duel18");
            textBox139.Text = GetEmptyStrings("AIMLBot.Duel19");
            textBox140.Text = GetEmptyStrings("AIMLBot.Duel20");

        }

        private void Form2_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.curse.com/downloads/wow-addons/details/auto-decline.aspx");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

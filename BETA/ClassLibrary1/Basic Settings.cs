﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Glider.Common.Objects;

namespace AIML
{
    public partial class Form1 : Form
    {

        static MonitorBox1 MonitorBox = new MonitorBox1();

        public Form1()
        {
            InitializeComponent();
        }

        public string NoEmptyforSave(string Input)
        {
            string tmp = Input;
            if (tmp.Length <= 0) tmp = ChatBot.EmplyReplacement;
            return tmp;
        }

        string CheckboxToString(bool tmp)
        {
            if (tmp) return "True"; else return "False";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GContext.Main.SetConfigValue("AIMLBot.StopGlide", CheckboxToString(checkBox1.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseFace", CheckboxToString(checkBox2.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.RandomMove", CheckboxToString(checkBox3.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.StopLoop", CheckboxToString(checkBox4.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.DWhisper", CheckboxToString(checkBox5.Checked) , true);
            GContext.Main.SetConfigValue("AIMLBot.UseBlacklist", CheckboxToString(checkBox6.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.GMReply", CheckboxToString(checkBox7.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseSay", CheckboxToString(checkBox8.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.TYBuff", CheckboxToString(checkBox9.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.TYHeal", CheckboxToString(checkBox10.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseInvite", CheckboxToString(checkBox11.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseInitiate", CheckboxToString(checkBox12.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseEmote", CheckboxToString(checkBox13.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseMisspell", CheckboxToString(checkBox14.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseMisspellCorrection", CheckboxToString(checkBox15.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseRemoveDot", CheckboxToString(checkBox16.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseBGchat", CheckboxToString(checkBox17.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.AIMLfeedback", CheckboxToString(checkBox18.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.BGEventUse", CheckboxToString(checkBox19.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseDuel", CheckboxToString(checkBox20.Checked), true);
            GContext.Main.SetConfigValue("AIMLBot.UseWhisper", CheckboxToString(checkBox21.Checked), true);

            GContext.Main.SetConfigValue("AIMLBot.CharDelay", numericUpDown1.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.JumpLike", numericUpDown2.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MaxGraylistCount", numericUpDown3.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MaxBlacklistCount", numericUpDown4.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.LookForCloseDistance", numericUpDown5.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.TYTime", numericUpDown6.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.InitiateDistance", numericUpDown7.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MSGTime", numericUpDown8.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MisspellLikelihood", numericUpDown9.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.BGTime", numericUpDown10.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MaxInvite", numericUpDown11.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.MaxWords", numericUpDown12.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.BGEventLikelihood", numericUpDown13.Value.ToString(), true);
            GContext.Main.SetConfigValue("AIMLBot.LookDelay", numericUpDown14.Value.ToString(), true);

            GContext.Main.SetConfigValue("AIMLBot.GMWhisper", textBox2.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.Keyword1", textBox3.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.Keyword2", textBox4.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.Keyword3", textBox5.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.Keyword4", textBox6.Text, true);
            GContext.Main.SetConfigValue("AIMLBot.MaxInviteMsg", textBox7.Text, true);

            GContext.Main.SetConfigValue("AIMLBot.DReply1", NoEmptyforSave(textBox1.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.DReply2", NoEmptyforSave(textBox8.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.DReply3", NoEmptyforSave(textBox9.Text), true);
            GContext.Main.SetConfigValue("AIMLBot.DReply4", NoEmptyforSave(textBox10.Text), true);

            ChatBot.GetSettings();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string GetEmptyStrings(string name)
        {
            string tmp = GContext.Main.GetConfigString(name);
            if (tmp == ChatBot.EmplyReplacement) tmp = "";
            return tmp;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = GContext.Main.GetConfigBool("AIMLBot.StopGlide");
            checkBox2.Checked = GContext.Main.GetConfigBool("AIMLBot.UseFace");
            checkBox3.Checked = GContext.Main.GetConfigBool("AIMLBot.RandomMove");
            checkBox4.Checked = GContext.Main.GetConfigBool("AIMLBot.StopLoop");
            checkBox5.Checked = GContext.Main.GetConfigBool("AIMLBot.DWhisper");
            checkBox6.Checked = GContext.Main.GetConfigBool("AIMLBot.UseBlacklist");
            checkBox7.Checked = GContext.Main.GetConfigBool("AIMLBot.GMReply");
            checkBox8.Checked = GContext.Main.GetConfigBool("AIMLBot.UseSay");
            checkBox9.Checked = GContext.Main.GetConfigBool("AIMLBot.TYBuff");
            checkBox10.Checked = GContext.Main.GetConfigBool("AIMLBot.TYHeal");      
            checkBox11.Checked = GContext.Main.GetConfigBool("AIMLBot.UseInvite");
            checkBox12.Checked = GContext.Main.GetConfigBool("AIMLBot.UseInitiate");
            checkBox13.Checked = GContext.Main.GetConfigBool("AIMLBot.UseEmote");
            checkBox14.Checked = GContext.Main.GetConfigBool("AIMLBot.UseMisspell");
            checkBox15.Checked = GContext.Main.GetConfigBool("AIMLBot.UseMisspellCorrection");     
            checkBox16.Checked = GContext.Main.GetConfigBool("AIMLBot.UseRemoveDot");
            checkBox17.Checked = GContext.Main.GetConfigBool("AIMLBot.UseBGchat");
            checkBox18.Checked = GContext.Main.GetConfigBool("AIMLBot.AIMLfeedback");
            checkBox19.Checked = GContext.Main.GetConfigBool("AIMLBot.BGEventUse");
            checkBox20.Checked = GContext.Main.GetConfigBool("AIMLBot.UseDuel");
            checkBox21.Checked = GContext.Main.GetConfigBool("AIMLBot.UseWhisper");
            
            numericUpDown1.Value = GContext.Main.GetConfigInt("AIMLBot.CharDelay");
            numericUpDown2.Value = GContext.Main.GetConfigInt("AIMLBot.JumpLike");
            numericUpDown3.Value = GContext.Main.GetConfigInt("AIMLBot.MaxGraylistCount");
            numericUpDown4.Value = GContext.Main.GetConfigInt("AIMLBot.MaxBlacklistCount");
            numericUpDown5.Value = GContext.Main.GetConfigInt("AIMLBot.LookForCloseDistance");
            numericUpDown6.Value = GContext.Main.GetConfigInt("AIMLBot.TYTime");
            numericUpDown7.Value = GContext.Main.GetConfigInt("AIMLBot.InitiateDistance");
            numericUpDown8.Value = GContext.Main.GetConfigInt("AIMLBot.MSGTime");
            numericUpDown9.Value = GContext.Main.GetConfigInt("AIMLBot.MisspellLikelihood");
            numericUpDown10.Value = GContext.Main.GetConfigInt("AIMLBot.BGTime");
            numericUpDown11.Value = GContext.Main.GetConfigInt("AIMLBot.MaxInvite");
            numericUpDown12.Value = GContext.Main.GetConfigInt("AIMLBot.MaxWords");
            numericUpDown13.Value = GContext.Main.GetConfigInt("AIMLBot.BGEventLikelihood");
            numericUpDown14.Value = GContext.Main.GetConfigInt("AIMLBot.LookDelay");

            textBox1.Text = GContext.Main.GetConfigString("AIMLBot.DReply");
            textBox2.Text = GContext.Main.GetConfigString("AIMLBot.GMWhisper");
            textBox3.Text = GContext.Main.GetConfigString("AIMLBot.Keyword1");
            textBox4.Text = GContext.Main.GetConfigString("AIMLBot.Keyword2");
            textBox5.Text = GContext.Main.GetConfigString("AIMLBot.Keyword3");
            textBox6.Text = GContext.Main.GetConfigString("AIMLBot.Keyword4");
            textBox7.Text = GContext.Main.GetConfigString("AIMLBot.MaxInviteMsg");

            textBox1.Text = GetEmptyStrings("AIMLBot.DReply1");
            textBox8.Text = GetEmptyStrings("AIMLBot.DReply2");
            textBox9.Text = GetEmptyStrings("AIMLBot.DReply3");
            textBox10.Text = GetEmptyStrings("AIMLBot.DReply4");

        }

        private void Form1_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ChatBot.StartHelpFile();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ChatBot.GetLatestAIML();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.curse.com/downloads/wow-addons/details/auto-decline.aspx");
        }
    }
}

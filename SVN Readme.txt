Hi all,

First of all thanks for getting the full SVN content...Hoping you will consider contributing to the code for future releases.

The Beta and Stable code are best viewed and made in MS-Visual Studio 2008 Express.. You can get it for free from Microsoft page : http://www.microsoft.com/Express/

When you like to edit and compile the installer file get the GNU ISTool from : http://www.istool.org/
When you like to edit and compile the Help Files get the West Wind Html Help Builder from : http://www.west-wind.com/

Good luck and Enjoy...

Cheers Zeo80.